package com.dl.demo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import android.widget.TextView;

public class SlidingDrawerDemo extends Activity {
	private SlidingDrawer mDrawer;
	private TextView imbg;
	private Button padopen,padclose;
	private boolean flag=false;
	 protected void onCreate(Bundle savedInstanceState) {
		  // TODO Auto-generated method stub
		  super.onCreate(savedInstanceState);
		  setContentView(R.layout.main);
		  
		  imbg=(TextView)findViewById(R.id.handle);
		  mDrawer=(SlidingDrawer)findViewById(R.id.slidingdrawer);
		  
		  padopen=(Button)findViewById(R.id.pad_btn);
		  padclose = (Button)findViewById(R.id.padc_btn);
		  mDrawer=(SlidingDrawer)findViewById(R.id.slidingdrawer);
		  
		  padopen.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mDrawer.open();
			}
		});
		  padclose.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mDrawer.close();
				}
			});
		  mDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener()
		  {
		   @Override
		   public void onDrawerOpened() {
		    //flag=true;
		   }
		   
		  });
		  
		  mDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener(){

		   @Override
		   public void onDrawerClosed() {
		    //flag=false;
		   }
		   
		  });
		  
		  mDrawer.setOnDrawerScrollListener(new SlidingDrawer.OnDrawerScrollListener(){

		   @Override
		   public void onScrollEnded() {
		   }

		   @Override
		   public void onScrollStarted() {
		   }
		   
		  });

		 
		 }

		}
